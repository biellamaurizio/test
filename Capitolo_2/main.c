#include <stdio.h>
#define PI_GRECO 3.14f
#define TAX_ADDED 5.0f

int main(int argc, char **argv)
{
	float volume;
	float raggio;
	float a;
	float b;
	
	printf("Parkinsons Law:\nWork expands so as to "); 
	printf("fill the time\n");
	printf("available for its completion.\n\n\n");
	
	printf("   *\n");
	printf("  *\n");
	printf(" *\n");
	printf("*****\n");
	
	volume = (4.0f/3.0f) * PI_GRECO * (10 * 10 * 10);
	printf("Il volume di una sfera di 10 metri di raggio e': %.2f\n", volume);
	printf("Che raggio ha la sfera?: ");
	scanf("%f", &raggio);
	volume = (4.0f/3.0f) * PI_GRECO * (raggio * raggio * raggio);
	printf("Il volume di una sfera di %.2f metri di raggio e': %.2f\n", raggio, volume);
	
	printf("Inserisci l'ammontare in euro: ");
	scanf("%f", &a);
	b = (TAX_ADDED/100) * a;
	b = a + b;
	printf("Il %.2f di %.2f e': %.2f\n", TAX_ADDED, a, b);
	
	return 0;
}
