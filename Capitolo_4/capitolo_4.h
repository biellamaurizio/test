void upc()
{
	int d, i1, i2, i3, i4, i5, j1, j2, j3, j4, j5, first_sum, second_sum, total;
	
	printf("Enter the first (single) digit: ");
	scanf("%1d", &d);
	printf("Enter first group of five digits: ");
	scanf("%1d%1d%1d%1d%1d", &i1, &i2, &i3, &i4, &i5);
	printf("Enter second group of five digits: ");
	scanf("%1d%1d%1d%1d%1d", &j1, &j2, &j3, &j4, &j5);
	
	first_sum = d + i2 + i4 + j1 + j3 + j5;
	second_sum = i1 + i3 +i5 + j2 + j4;
	total = 3 * first_sum + second_sum;
	
	printf("Check digit: %d\n", 9 -((total - 1) % 10));
}
void incdec()
{
	int i = 10;
	printf("valore di i: %d\n", i++);
	printf("valore di i: %d\n", i);
}
void es1()
{
	int i = 5;
	int j = 3;
	printf("%d %d\n", i/j, i%j);
	i = 2;
	j = 3;
	printf("%d\n", (i+10) % j);
	i=7; j=8; int k=9;
	printf("%d\n", (i+10) % k / j);
	i=1; j=2; k=3;
	printf("%d\n", (i+5) % (j+2) / k);
}
void es2()
{
	int i=4; int j=2;
	printf("%d %d\n", (-i)/j, -(i/j));
}
void es11()
{
	int i = 1;
	printf("%d\n", i++ - 1);
	printf("%d\n", i);
}
void prog1()
{
	int i, a, b;
	printf("Enter a two-digit number: ");
	scanf("%d", &i);
	a=i%10;
	b=i/10;
	printf("The reversal is: %d%d\n", a,b);
}
void prog2()
{
	int i, a, b, c;
	printf("Enter a three-digit number: ");
	scanf("%d", &i);
	a=i/100;
	b=(i/10)%10;
	c=i%10;
	printf("The reversal is: %d-%d-%d\n", c,b,a);
}
void prog3()
{
	int a, b, c;
	printf("Enter a three-digit number: ");
	scanf("%1d%1d%1d", &a, &b, &c);
	printf("The reverse is: %d%d%d\n", c, b, a);
}
void prog4()
{
	int i;
	printf("Enter a number between 0 and 32767: \n");
	scanf("%d", &i);
	printf("In octal, your number is: %5.5o\n", i);
	
}