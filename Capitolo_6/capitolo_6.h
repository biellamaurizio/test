void square()
{
	/* Stampa una tavola dei quadrati utilizzando l'istruzione while */
	
	int i, n;
	
	printf("This program prints a table of squares\n");
	printf("Enter number of entries in table: ");
	scanf("%d", &n);
	
	i=1;
	while(i <= n)
	{
		printf("%10d%10d\n", i, i*i);
		i++;
	}
}
void sum()
{
	/* Somma una sequenza di numeri */
	
	int n, sum=0;
	
	printf("This program sum a series of integers.\n");
	printf("Enter a integers (0 to terminate): ");
	scanf("%d", &n);
	while(n != 0)
	{
		sum += n;
		scanf("%d", &n);
	}
	printf("The sum is: %d\n", sum);
}
void numdigits()
{
	/* Calcola il numero di cifre di un numero intero. */
	int digits=0, n;
	
	printf("Enter a nonnegative integer: ");
	scanf("%d", &n);
	
	do
	{
		n = n/10;
		digits++;
	}while(n>0);
	
	printf("The number has %d digit(s).\n", digits);
}
void square2()
{
	/* Stampa una tavola dei quadrati usando un ciclo for. */
	
	int i, n;
	
	printf("This program print a table of squares.\n");
	printf("Enter number of entries in table: ");
	scanf("%d", &n);
	
	for(i=1; i<=n; i++)
	{
		printf("%10d%10d\n", i, i*i);
	}
}
void square3()
{
	/* Stampa una tavola dei quadrati usando un metodo strano. */
	
	int i, n, odd, square;
	
	printf("This program print a table of squares.\n");
	printf("Enter number of entries in table: ");
	scanf("%d", &n);
	
	i=1;
	odd=3;
	for(square=1; i<=n; odd+=2)
	{
		printf("%10d%10d\n", i, square);
		++i;
		square += odd;
	}
}
int checking()
{
	int cmd;
	float balance = 0.0f, credit, debit;
	
	printf("*** ACME checkbook-balancing program ***\n");
	printf("Commands: 0=clear, 1=credit, 2=debit, ");
	printf("3=balance, 4=exit\n\n");
	
	for(;;)
	{
		printf("Enter commanD: ");
		scanf("%d", &cmd);
		switch(cmd)
		{
			case 0:
				balance=0.0f;
				break;
			case 1:
				printf("Enter amount of credit: ");
				scanf("%f", &credit);
				balance += credit;
				break;
			case 2:
				printf("Enter amount of debit: ");
				scanf("%f", &debit);
				balance -= debit;
				break;
			case 3:
				printf("Current balance: $%.2f\n", balance);
				break;
			case 4:
				return 0;
				break;
			default:
				printf("Commands: 0=clear, 1=credit, 2=debit, ");
				printf("3=balance, 4=exit\n\n");
				break;
		}
		
	}
}
