
void tprintf()
{
	
	int i;
	float x;

	i=40;
	x = 839.21f;

	printf("|%d|%5d|%-5d|%5.5d|\n", i, i, i, i);
	printf("|%10.3f|%10.3e|%-10g|\a\a\n", x, x, x);
}

void addfrac()
{
	/* Sommare due frazioni */
	int num1, denom1, num2, denom2, result_num, result_denom;
	
	printf("Enter first fraction: ");
	scanf("%d/%d", &num1, &denom1);
	
	printf("Enter second fraction: ");
	scanf("%d/%d", &num2, &denom2);
	
	result_num = num1 * denom2 + num2 * denom1;
	result_denom = denom1 * denom2;
	
	printf("The sum is %d/%d\n", result_num, result_denom);
}

void esercizio1()
{
	int giorno, mese, anno;
	
	printf("Inserisci la data nel formato mm/dd/yyyy: ");
	scanf("%d/%d/%d", &mese, &giorno, &anno);
	
	printf("La data inserita e': %4.4d%2.2d%2.2d\n", anno, mese, giorno);
}

void esercizio3()
{
	int GS1, Group, Publisher, Item, Check;
	
	printf("Enter ISBN: ");
	scanf("%d-%d-%d-%d-%d", &GS1, &Group, &Publisher, &Item, &Check);
	printf("GS1 prefix: %d\n", GS1);
	printf("Group Identifier: %d\n", Group);
	printf("Publisher code: %d\n", Publisher);
	printf("Item number: %d\n", Item);
	printf("Check digit: %d\n", Check);
}

void esercizio4()
{
	int prefix, first, second;
	
	printf("Enter phone number [(xxx) xxx-xxxx]: ");
	scanf("(%d) %d-%d", &prefix, &first, &second);
	
	printf("Phone number: %d.%d.%d\n", prefix, first, second);
}

void esercizio5()
{
	int n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, rows1, rows2, rows3, rows4, col1, col2, col3, col4, diag1, diag2;
	
	printf("Enter 16 digit from 1 to 16 in any order: ");
	scanf("%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d", &n1, &n2, &n3, &n4, &n5, &n6, &n7, &n8, &n9, &n10, &n11, &n12, &n13, &n14, &n15, &n16);
	printf("%3d%3d%3d%3d\n", n1, n2, n3, n4);
	printf("%3d%3d%3d%3d\n", n5, n6, n7, n8);
	printf("%3d%3d%3d%3d\n", n9, n10, n11, n12);
	printf("%3d%3d%3d%3d\n", n13, n14, n15, n16);
	rows1 = n1+n2+n3+n4;
	rows2 = n5+n6+n7+n8;
	rows3 = n9+n10+n11+n12;
	rows4 = n13+n14+n15+n16;
	col1 = n1+n5+n9+n13;
	col2 = n2+n6+n10+n14;
	col3 = n3+n7+n11+n15;
	col4 = n4+n8+n12+n16;
	diag1 = n1+n6+n11+n16;
	diag2 = n4+n7+n10+n13;
	printf("Rows sums: %3d%3d%3d%3d\n", rows1, rows2, rows3, rows4);
	printf("Coloumn sums: %3d%3d%3d%3d\n", col1, col2, col3, col4);
	printf("Diagonal sums: %3d%3d\n", diag1, diag2);
	
}
