void broker()
{
	float commission, value;
	printf("Enter value of trade: ");
	scanf("%f", &value);
	
	if (value < 2500.00f)
		commission = 30.00f + .017f * value;
	else if (value < 6250.00f)
		commission = 56.00 + .0066f * value;
	else if (value < 20000.00f)
		commission = 76.00f + .0034f * value;
	else if (value < 50000.00f)
		commission = 100.00f + .0022f * value;
	else if (value < 500000.00f)
		commission = 155.00f + .0011f * value;
	else
		commission = 255.00f + .0009f * value;
	
	if (commission < 39.00f)
		commission = 39.00f;
	
	printf("Commission: $%.2f\n", commission);
}
void date()
{
	/* Stampa la data nel formato legale */
	int month, day, year;
	
	printf("Enter date (mm/dd/yy): ");
	scanf("%d /%d /%d", &month, &day, &year);
	
	printf("Dated this %d", day);
	switch(day)
	{
		case 1: case 21: case 31:
			printf("st");
			break;
		case 2: case 22:
			printf("nd");
			break;
		case 3: case 23:
			printf("rd");
			break;
		default:
			printf("th");
			break;
	}
	printf(" day of ");
	
	switch(month)
	{
		case 1: printf("January"); break;
		case 2: printf("February"); break;
		case 3: printf("March"); break;
		case 4: printf("April"); break;
		case 5: printf("May"); break;
		case 6: printf("June"); break;
		case 7: printf("July"); break;
		case 8: printf("August"); break;
		case 9: printf("September"); break;
		case 10: printf("October"); break;
		case 11: printf("November"); break;
		case 12: printf("December"); break;
	}
	printf(", 20%.2d.\n", year);
}
void prog1()
{
	int number;
	
	printf("Enter a number (max 3 digit): ");
	scanf("%d", &number);
	
	if (number < 10)
		printf("The number %d has 1 digit\n", number);
	else if(number < 100)
		printf(" The number %d has 2 digit\n", number);
	else
		printf(" The number %d has 3 digit\n", number);
}